from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from web.models import LessonQuestion, LessonTest, LessonAnswer 



def dashboard(request):
	questions = LessonQuestion.objects.all()
	if request.method == 'POST':
		all_questions_attended = True
		mark = 0
		wrong = 0
		
		for question in questions:
			answer_pk = request.POST.get('answer_' + str(question.pk))

			if not answer_pk:
				all_questions_attended = False

			if LessonAnswer.objects.filter(pk=answer_pk,question=question.pk).exists():

				answer = get_object_or_404(LessonAnswer.objects.filter(pk=answer_pk,question=question.pk))
				
				if answer.is_right_answer:
					mark = mark + 1	

				else:
					wrong = wrong + 1			


		if all_questions_attended:
			test = LessonTest(				
				test_mark = mark,
				neg_mark = wrong 
			).save()

			total = mark + wrong

			context = {
				"title" : "Result",
				'mark' : mark,
				'wrong' : wrong,
				'total' : total
			}

			return render(request,'web/result.html', context)
		else:
			context = {
				"title" : "Online Exam", 
				'questions': questions
			}
			return render(request,'web/exam.html', context)

	else:
		context = {
			"title" : "Dashboard",
			'questions': questions
		}
		return render(request,'web/exam.html', context)
