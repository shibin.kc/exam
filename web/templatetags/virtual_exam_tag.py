from django.template import Library 
register = Library()
from django.template.defaultfilters import stringfilter
from web.models import LessonTest, LessonAnswer, LessonQuestion

@register.filter
def get_answers(question):
	answers =  LessonAnswer.objects.filter(question=question)
	return answers


 
